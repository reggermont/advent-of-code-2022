import { createReadStream } from 'fs';
import { createInterface } from 'readline';

function day1Part1(path: string) {
  let current = 0;
  let max = 0;

  const stream = createInterface({
    input: createReadStream(path)
  });

  stream.on('line', function (line) {
    if (line === '') {
      max = current > max ? current : max
      current = 0
    } else {
      current += parseInt(line)
    }
  });

  stream.on('close', function () {
    console.log(max)
  });

}

function day1Part2(path: string) {
  const caloriesPerElve: Array<number> = [0];
  let index = 0

  const stream = createInterface({
    input: createReadStream(path)
  });

  stream.on('line', function (line) {
    if (line === '') {
      index += 1
      caloriesPerElve[index] = 0
    } else {
      caloriesPerElve[index] += parseInt(line)
    }
  });

  stream.on('close', function () {
    caloriesPerElve.sort(function (a, b) { return b - a });
    console.log(caloriesPerElve[0] + caloriesPerElve[1] + caloriesPerElve[2])
  });

}

day1Part1('../inputs/day1.txt');
day1Part2('../inputs/day1.txt');
